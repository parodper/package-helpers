#!/bin/sh
#
#    Copyright (C) 2019  Mason Hock <mason@masonhock.com>
#    Copyright (C) 2021,2022  Ruben Rodriguez <ruben@trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=16

. ./config

sed -i 's/manjaro/trisquel/g' mate-tweak
sed -i 's/Manjaro/Trisquel/g' mate-tweak

# Set icon
sed 's/Icon=.*/Icon=mate-desktop-symbolic/' -i data/mate-tweak.desktop

# Disable composition on llvm
sed 's/Software Rasterizer/Accelerated: no/' -i mate-tweak

# Corrects rendering delay on Pluma, possibly others
sed '/--backend/s/\\/--xrender-sync-fence \\/' -i  marco-wrapper

# Shadow config
sed -i '/shadow-radius/s|12|15|' marco-wrapper
sed -i '/shadow-opacity/s|0.125|0.5|' marco-wrapper
sed -i '/shadow-offset-x/s|-12|-15|' marco-wrapper
sed -i '/shadow-offset-y/s|-12|-15|' marco-wrapper
sed -i 's/Firefox/abrowser/' marco-wrapper
sed -i 's/Ulauncher/icedove/' marco-wrapper

# Transitional dummy marco-compton
rm marco-compton
cat << EOF > marco-compton
#!/bin/sh

# Compton has been replaced by picom (marco-xrender wrapper)

WINDOW_MANAGER=xterm
[ -f /usr/bin/marco ] && WINDOW_MANAGER=marco
[ -f /usr/bin/marco-no-composite ] && WINDOW_MANAGER=marco-no-composite
gsettings set org.mate.Marco.general compositing-manager false

# Check for hardware acceleration
if glxinfo -B | grep "OpenGL renderer string" | grep -qv llvmpipe ; then
  WINDOW_MANAGER=marco
  gsettings set org.mate.Marco.general compositing-manager true
  # Use marco-glx (picom) on non-intel gpu, if available
  if ! glxinfo -B | grep -qi vendor.*intel; then
    if [ -f /usr/bin/picom ] && [ -f /usr/bin/marco-glx ] ; then
            WINDOW_MANAGER=marco-glx
    fi
  fi
fi

gsettings set org.mate.session.required-components windowmanager \$WINDOW_MANAGER

exec \$WINDOW_MANAGER
EOF
chmod 755 marco-compton

cat << EOF > data/marco-compton.desktop
[Desktop Entry]
Type=Application
Name=Marco Compton
Exec=marco-compton
NoDisplay=true
# name of loadable control center module
X-MATE-WMSettingsModule=marco
# name we put on the WM spec check window
X-MATE-WMName=Marco
# back compat only
X-MateWMSettingsLibrary=marco
X-MATE-Bugzilla-Bugzilla=MATE
X-MATE-Bugzilla-Product=marco
X-MATE-Bugzilla-Component=general
X-GNOME-Autostart-Phase=WindowManager
X-MATE-Autostart-Phase=WindowManager
X-GNOME-Provides=windowmanager
X-MATE-Provides=windowmanager
X-GNOME-Autostart-Notify=true
X-MATE-Autostart-Notify=true
EOF
sed "/marco-no-composite.desktop/a\ \ \ \ ('{prefix}/share/applications'.format(prefix=sys.prefix), ['data/marco-compton.desktop',])," -i setup.py

# Add Trisquel to layout list
sed 's/fedora/trisquel/g; s/Fedora/Trisquel/' -i mate-tweak

changelog "Add Trisquel as panel layout option"

compile

