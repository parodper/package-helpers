#!/bin/sh
#
#    Copyright (C) 2023 Luis Guzman <ark@switnet.org>
#    Copyright (C) 2008-2023  Ruben Rodriguez <ruben@trisquel.info>
#    Copyright (C) 2019 David Trudgian <dave@trudgian.net>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=24

. ./config

PRESERVEDIRS='
drivers/bluetooth
drivers/gpu/drm/i915
drivers/gpu/drm/amd/amdgpu
drivers/gpu/drm/amd/pm/powerplay
drivers/gpu/drm/r128
drivers/gpu/drm/radeon
drivers/net/wireless/intel/ipw2x00
drivers/net/wireless/intel/iwlegacy
drivers/net/wireless/intel/iwlwifi
drivers/net/wireless/realtek/rtl8xxxu
drivers/net/wireless/realtek/rtlwifi
drivers/net/wireless/realtek/rtlwifi/rtl8188ee
drivers/net/wireless/realtek/rtlwifi/rtl8192ce
drivers/net/wireless/realtek/rtlwifi/rtl8192cu
drivers/net/wireless/realtek/rtlwifi/rtl8192de
drivers/net/wireless/realtek/rtlwifi/rtl8192ee
drivers/net/wireless/realtek/rtlwifi/rtl8192se
drivers/net/wireless/realtek/rtlwifi/rtl8723ae
drivers/net/wireless/realtek/rtlwifi/rtl8723be
drivers/net/wireless/realtek/rtlwifi/rtl8821ae
drivers/net/wireless/broadcom/brcm80211
'

TMPDIR=$(mktemp -d preserve-XXXX)
PRESERVE=$(grep '^+++' $DATA/silent-accept-firmware.patch | /bin/sed 's/+++ //; s/\t.*//;' | cut -d/ -f2- | sort -u )
for FILE in $PRESERVE; do
  cp $FILE $TMPDIR --parents -a
done
for DIR in $PRESERVEDIRS; do
  cp $DIR $TMPDIR --parents -a
done

sh $DATA/deblob-5.15

echo "Reverting deblobbing for files patched by silent-accept-firmware"
cp $TMPDIR/* . -av
rm -rf $TMPDIR

for PATCH in $DATA/*.patch ; do
  echo $PATCH
  patch --no-backup-if-mismatch -p1 < $PATCH
done

# Simple test for finding missing cases of silent_accept_firmware method
#for dir in $PRESERVEDIRS; do
#  grep -i "load.*firmware.*%" $PRESERVEDIRS -r
#  grep -i "load.*ucode.*%" $PRESERVEDIRS -r
#done

# Re-enable udebs
cp $DATA/5-udebs.mk debian/rules.d
cp -a $DATA/d-i debian.master
cp $DATA/kernel-wedge-arch.pl debian/scripts/misc/

cat << EOF >> debian/control
Package: linux-udebs-generic
Build-Profiles: <!stage1>
XC-Package-Type: udeb
Section: debian-installer
Architecture: amd64 armhf arm64 ppc64el s390x
Depends: \${udeb:Depends}
Description: Metapackage depending on kernel udebs
 This package depends on the all udebs that the kernel build generated,
 for easier version and migration tracking.

Package: linux-udebs-generic-lpae
Build-Profiles: <!stage1>
XC-Package-Type: udeb
Section: debian-installer
Architecture: armhf
Depends: \${udeb:Depends}
Description: Metapackage depending on kernel udebs
 This package depends on the all udebs that the kernel build generated,
 for easier version and migration tracking.

Package: linux-udebs-lowlatency
Build-Profiles: <!stage1>
XC-Package-Type: udeb
Section: debian-installer
Architecture: amd64
Depends: \${udeb:Depends}
Description: Metapackage depending on kernel udebs
 This package depends on the all udebs that the kernel build generated,
 for easier version and migration tracking.
EOF

cat << EOF >> debian.master/control.d/flavour-control.stub
Package: linux-udebs-FLAVOUR
Build-Profiles: <!stage1>
XC-Package-Type: udeb
Section: debian-installer
Architecture: ARCH
Depends: \${udeb:Depends}
Description: Metapackage depending on kernel udebs
 This package depends on the all udebs that the kernel build generated,
 for easier version and migration tracking.
EOF

sed '/include.*2-binary-arch.mk/a\\n# Rules for building the udebs ($(DEBIAN)-installer)\ninclude $(DROOT)/rules.d/5-udebs.mk' -i debian/rules
sed 's/+= binary-debs/+= binary-udebs/' -i debian/rules.d/2-binary-arch.mk
sed '/Build-Depends:/a\ kernel-wedge <!stage1>,' -i 	debian/control \
							debian.master/control.stub.in

# Wipe dkms-versions
# lists zfs / v4l2loopback versions
echo > debian/dkms-versions

# Remove ZFS
rm zfs spl debian/scripts/misc/update-zfs.sh -rf
/bin/sed 's/spl-dkms, zfs-dkms//' -i debian/control \
                                     debian.master/control.d/vars.generic \
                                     debian.master/control.d/vars.*

/bin/sed -i '/ifeq ($(do_zfs),false)/,/endif$/d' debian/rules
/bin/sed  -i '/zfs/d' debian.master/abi/*/*.modules \
                      debian/rules.d/2-binary-arch.mk \
                      debian.master/rules.d/* \
                      debian/rules
#                      debian.master/d-i/modules/fs-core-modules \
#                      debian.master/control.d/generic.inclusion-list \
#                      debian.master/control.d/vars.*

# Remove VBox
#sed -i '/vbox/s|true|false|' debian.master/rules.d/amd64.mk

# Remove v4l2loopback
/bin/sed -i '/ifeq ($(do_v4l2loopback),false)/,/endif$/d' debian/rules
/bin/sed -i '/v4l2loopback/d' debian/rules.d/2-binary-arch.mk \
                              debian/rules
/bin/sed -i '/do_v4l2loopback/d' debian.master/rules.d/*

# Remove nvidia
rm -rf debian/scripts/dkms-build--nvidia-N
sed -i '/dkms-build--nvidia-N/d' debian.master/reconstruct

#/bin/sed  '/do_dkms_nvidia = true/d' -i debian.master/abi/*/*/*.modules \
#                                        debian/rules \
#                                        debian.master/control.d/vars.*

# Remove wireguard
/bin/sed '/ifeq ($(do_dkms_wireguard),false)/,/endif$/d' -i debian/rules
#sed '/do_dkms_wireguard/d' -i debian/rules.d/2-binary-arch.mk
#/bin/sed '/do_dkms_wireguard/d' -i debian.master/rules.d/*

# Compile with less modules and avoid abi check
echo 'skipmodule = true' >> debian.master/rules.d/0-common-vars.mk
echo 'skipabi = true' >> debian.master/rules.d/0-common-vars.mk
echo 'skipmodule = true' >> debian/rules.d/0-common-vars.mk
echo 'skipabi = true' >> debian/rules.d/0-common-vars.mk

# Skip the retpoline check as there is no last release to chaeck against
echo 'skipretpoline = true' >> debian.master/rules.d/0-common-vars.mk
echo 'skipretpoline = true' >> debian/rules.d/0-common-vars.mk

# Do not label packages as unsigned
sed '/bin_pkg_name_unsigned/s/linux-image-unsigned/linux-image/' -i debian/rules.d/0-common-vars.mk
sed 's/.unsigned//' -i debian/scripts/control-create

line=$(grep -n ')-Ubuntu' debian/rules.d/0-common-vars.mk|cut -d: -f1)
sed $(expr $line - 1 ),$(expr $line + 1 )d debian/rules.d/0-common-vars.mk -i
sed s/family=ubuntu/family=trisquel/ -i debian/rules.d/0-common-vars.mk
cat << EOF > debian.master/etc/kernelconfig
archs="amd64 i386 armhf arm64 ppc64el"
family='trisquel'
EOF

rename s/ubuntu/trisquel/ debian.*/config/config.common.ubuntu
grep -lr config.common.ubuntu debian*/ | xargs sed -i 's|config.common.ubuntu|config.common.trisquel|g'

find debian* -type f -name *control* -exec sed 's/ with Ubuntu patches//; s/Linux/Linux-libre/g' -i {} \;

# Descriptions should not change based on the build arch
sed 's/on DESC//; s/PKGVER on/PKGVER/; /^ DESC.$/d;' debian*/control.d/flavour-control.stub -i

sed '/^firmware/d' ./debian*/abi/fwinfo -i
#echo > ./debian.master/d-i/firmware/nic-modules
#echo > ./debian.master/d-i/firmware/scsi-modules

# Disable using udev as a fallback for firmware loading
replace "CONFIG_FW_LOADER_USER_HELPER=y" "CONFIG_FW_LOADER_USER_HELPER=n" debian.master/config

# Fix ports build
sed -i "/CONFIG_FW_LOADER_USER_HELPER/s|'armhf': 'y',|'armhf': 'n',|g" debian.master/config/annotations
sed -i "/CONFIG_FW_LOADER_USER_HELPER/s|'arm64': 'y',|'arm64': 'n',|g" debian.master/config/annotations
sed -i "/CONFIG_FW_LOADER_USER_HELPER/s|'ppc64el': 'y',|'ppc64el': 'n',|g" debian.master/config/annotations
sed -i "/CONFIG_FW_LOADER_USER_HELPER_FALLBACK/s|'armhf': 'n',|'armhf': '-',|g" debian.master/config/annotations
sed -i "/CONFIG_FW_LOADER_USER_HELPER_FALLBACK/s|'arm64': 'n',|'arm64': '-',|g" debian.master/config/annotations
sed -i "/CONFIG_FW_LOADER_USER_HELPER_FALLBACK/s|'ppc64el': 'n',|'ppc64el': '-',|g" debian.master/config/annotations

# Disable aaeon.
sed -i "/AAEON/d" debian.master/config/annotations
# Disable ubuntu ODM drivers
sed -i "/CONFIG_UBUNTU_ODM_DRIVERS/d" debian.master/config/annotations

cp debian.master/config debian.hwe -a

changelog "Removed non-free bits"

cp debian/changelog debian.master/changelog
cp debian/changelog debian.hwe/changelog

compile
