Package: kernel-image
Provides: ext3-modules, ext4-modules, squashfs-modules
Provides_amd64: efi-modules, ext3-modules, ext4-modules, squashfs-modules
Provides_i386: efi-modules, ext3-modules, ext4-modules, squashfs-modules
Provides_ppc64el: ext3-modules, ext4-modules, fat-modules, squashfs-modules, i2c-modules
Provides_s390x: ext3-modules, ext4-modules, ppp-modules, squashfs-modules
Description: kernel image and system map

Package: dasd-modules
Depends: kernel-image, storage-core-modules
Priority: standard
Description: DASD storage support

Package: dasd-extra-modules
Depends: dasd-modules
Priority: extra
Description: DASD storage support -- extras

Package: fat-modules
Depends: kernel-image
Priority: optional
Priority_amd64: standard
Priority_i386: standard
Description: FAT filesystem support
 This includes Windows FAT and VFAT support.

Package: fb-modules
Depends: kernel-image, i2c-modules
Priority: standard
Description: Framebuffer modules

Package: firewire-core-modules
Depends: kernel-image, storage-core-modules, crc-modules
Priority: standard
Description: Firewire (IEEE-1394) Support

Package: floppy-modules
Depends: kernel-image
Priority: standard
Description: Floppy driver support

Package: fs-core-modules
Depends: kernel-image
Priority: standard
Provides: ext2-modules, jfs-modules, reiserfs-modules, xfs-modules
Description: Base filesystem modules
 This includes jfs, reiserfs and xfs.

Package: fs-secondary-modules
Depends: kernel-image, fat-modules, cdrom-core-modules
Priority: standard
Provides: btrfs-modules, ntfs-modules, hfs-modules
Description: Extra filesystem modules
 This includes support for Windows NTFS and MacOS HFS/HFSPlus

Package: input-modules
Depends: kernel-image, usb-modules, crc-modules, i2c-modules
Priority: standard
Description: Support for various input methods

Package: md-modules
Depends: kernel-image
Priority: standard
Provides: crypto-dm-modules
Description: Multi-device support (raid, device-mapper, lvm)

Package: nic-modules
Depends: kernel-image, nic-shared-modules, virtio-modules, i2c-modules, crc-modules, mtd-core-modules
Priority: standard
Description: Network interface support

Package: nic-wireless-modules
Depends: kernel-image, nic-shared-modules, usb-modules, pcmcia-modules, crc-modules, crypto-modules
Priority: standard
Description: Wireless NIC drivers
 This package contains wireless NIC drivers for the kernel.
 Includes crypto modules only needed for wireless (WEP, WPA).

Package: nic-pcmcia-modules
Depends: kernel-image, nic-shared-modules, nic-modules
Priority: standard
Description: PCMCIA network interface support

Package: nic-usb-modules
Depends: kernel-image, nic-shared-modules, usb-modules
Priority: standard
Description: USB network interface support

Package: nic-shared-modules
Depends: kernel-image, crypto-modules
Priority: standard
Description: nic shared modules
  This package contains modules which support nic modules

Package: parport-modules
Depends: kernel-image
Priority: standard
Description: Parallel port support

Package: pata-modules
Depends: kernel-image, storage-core-modules
Priority: standard
Description: PATA support modules

Package: pcmcia-modules
Depends: kernel-image
Priority: standard
Description: PCMCIA Modules

Package: pcmcia-storage-modules
Depends: kernel-image, scsi-modules
Priority: standard
Description: PCMCIA storage support

Package: plip-modules
Depends: kernel-image, nic-shared-modules, parport-modules
Priority: standard
Description: PLIP (parallel port) networking support

Package: ppp-modules
Depends: kernel-image, nic-shared-modules, serial-modules, crc-modules
Priority: standard
Description: PPP (serial port) networking support

Package: sata-modules
Depends: kernel-image, storage-core-modules, scsi-core-modules
Priority: standard
Description: SATA storage support

Package: scsi-modules
Depends: kernel-image, storage-core-modules, scsi-core-modules
Priority: standard
Description: SCSI storage support

Package: serial-modules
Depends: kernel-image, pcmcia-modules
Priority: standard
Description: Serial port support

Package: storage-core-modules
Depends: kernel-image
Priority: standard
Provides: loop-modules
Description: Core storage support
 Includes core SCSI, LibATA, USB-Storage. Also includes related block
 devices for CD, Disk and Tape medium (and IDE Floppy).

Package: usb-modules
Depends: kernel-image, storage-core-modules
Priority: standard
Description: Core USB support

Package: nfs-modules
Priority: standard
Depends: kernel-image
Description: NFS filesystem drivers
 Includes the NFS client driver, and supporting modules.

Package: block-modules
Priority: standard
Provides: nbd-modules
Depends: kernel-image, storage-core-modules, parport-modules, virtio-modules
Description: Block storage devices
 This package contains the block storage devices, including DAC960 and
 paraide.

Package: message-modules
Priority: standard
Depends: kernel-image, storage-core-modules, scsi-modules
Description: Fusion and i2o storage modules
 This package containes the fusion and i2o storage modules.

Package: crc-modules
Depends: kernel-image
Priority: optional
Description: CRC modules
 This package contains CRC support modules.

Package: crypto-modules
Priority: extra
Depends: kernel-image
Description: crypto modules
 This package contains crypto modules.

Package: virtio-modules
Priority: standard
Depends: kernel-image
Description: VirtIO Modules
 Includes modules for VirtIO (virtual machine, generally kvm guests)

Package: socket-modules
Depends: kernel-image
Priority: standard
Description: Unix socket support

Package: mouse-modules
Depends: kernel-image, input-modules, usb-modules
Priority: extra
Description: Mouse support
 This package contains mouse drivers for the Linux kernel.

Package: vlan-modules
Depends: kernel-image
Priority: extra
Description: vlan modules
 This package contains vlan (8021.Q) modules.

Package: ipmi-modules
Depends: kernel-image
Priority: standard
Description: ipmi modules

Package: multipath-modules
Depends: kernel-image, scsi-core-modules
Priority: extra
Description: DM-Multipath support
  This package contains modules for device-mapper multipath support.

Package: isofs-modules
Depends: kernel-image, cdrom-core-modules
Priority: standard
Description: ISOFS filesystem support
 This package contains the ISOFS filesystem module for the kernel.

Package: usb-storage-modules
Depends: kernel-image, scsi-core-modules, usb-modules
Priority: standard
Description: USB storage support
 This package contains the USB storage driver for the kernel.

Package: scsi-core-modules
Depends: kernel-image
Provides: cdrom-core-modules
Priority: standard
Description: Core SCSI subsystem
 This package contains the core SCSI subsystem for the kernel.

Package: i2c-modules
Depends: kernel-image
Priority: optional
Description: i2c support modules
 This package contains basic i2c support modules

Package: mtd-modules
Depends: kernel-image, mtd-core-modules
Priority: optional
Description: MTD driver modules
 This package contains MTD driver modules.

Package: mtd-core-modules
Depends: kernel-image
Priority: optional
Description: MTD core
 This package contains the MTD core.

